from weapon import Weapon

class Granada(Weapon):
    def __init__(self, duenio):
        # clase granada hija de weapon, tiene daño 3
        self._damage=3
        super().__init__(duenio)
    @property
    def damage(self):
        return self._damage
        
    