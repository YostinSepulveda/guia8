from weapon import Weapon

class Mano(Weapon):
    def __init__(self, duenio):
        # clase mano hija de weapon, tiene daño 0
        self._damage=0
        super().__init__(duenio)
    @property
    def damage(self):
        return self._damage