from weapon import Weapon

class Canon(Weapon):
    def __init__(self,duenio):
        # clase cañon hija de weapon, tiene daño 2
        self._damage=2
        super().__init__(duenio)
    @property
    def damage(self):
        return self._damage
        
    