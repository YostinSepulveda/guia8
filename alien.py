import random
from weapon import Weapon
class Alien():
    def __init__(self,x,y):
        # el alien tiene arma, posicion y vida
        self._x=x
        self._y=y
        self._health=3
        self._weapon= None

    # setters y getters de los atributos
    @property
    def health(self):
        return self._health
    
    @health.setter
    def health(self,new_health):
        self._health=new_health
    
    @property
    def x(self):
        return self._x
    
    @x.setter
    def x(self,new_x):
        self._x = new_x
        
    @property
    def y(self):
        return self._y
    
    @y.setter
    def y(self,new_y):
        self._y = new_y
    
    @property
    def weapon(self):
        return self._weapon
    
    @weapon.setter
    def weapon(self,weapon):
        if isinstance(weapon,Weapon):
            self._weapon = weapon
            
    def is_alive(self):
        # revisa si el alien sigue vivo o no
        if self.health <= 0:
            return False
        return True
    
    def teleport(self):
        # teletransporta al alien a una posicion diferente a la que ya tenia
        new_x = random.randint(0, 4)
        new_y = random.randint(0, 4)
        while new_x == self.x and new_y == self.y:
            new_x = random.randint(0, 4)
            new_y = random.randint(0, 4)
        self.x = new_x
        self.y = new_y
        print("El alien se ha teletransportado")
        
        
    def hit(self,obj):
        # el alien ataca a otro con su arma
        print("El alien ha atacado")
        obj.health = obj.health - self.weapon.damage
        
        
    
                
        
        