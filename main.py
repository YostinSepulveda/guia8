# se importan los modulos necesarios
import random
from alien import Alien
from pistola_laser import Pistola
from cañon_protones import Canon
from granada import Granada
from mano import Mano

class Juego():
    def __init__(self):
        # se crea la matriz espacio 5x5, lista de aliens y total
        self._espacio=[[], [], [], [], []]
        self._aliens=[]
        self._total_aliens = 0
    
    # setters y getters de los atributos de la clase
    @property
    def aliens(self):
        return self._aliens
        
    @aliens.setter
    def aliens(self,alien):
        if isinstance(alien,Alien):
            self._aliens.append(alien)
    
    @property
    def espacio(self):
        return self._espacio
    
    @espacio.setter
    def espacio(self, reset):
        self._espacio = reset
    
    @property
    def total_aliens(self):
        return self._total_aliens
    
    @total_aliens.setter
    def total_aliens(self, cant):
        self._total_aliens = cant
    
    def crear_aliens(self):
        # se crea cantidad random de aliens y se agregan a la lista
        cant_aliens = random.randint(2, 10)
        coords = []
        for i in range(cant_aliens):
            x = random.randint(0, 4)
            y = random.randint(0,4)
            while (x, y) in coords:
                x = random.randint(0, 4)
                y = random.randint(0,4)
            coords.append((x,y))
        
        for coord in coords:
            alien = Alien(coord[0], coord[1])
            self.aliens.append(alien)
        self.total_aliens = len(self.aliens)
        
    def detecta_colision(self):
        # se detectan aliens que ocupen el mismo lugar, se asesina a uno.
        elem=0
        coords = []
        for alien in self.aliens:
            coords.append((alien.x, alien.y))
        for coord in coords:
            if coords.count(coord) > 1:
                elem = coord
                break
        if elem == 0:
            return
        pos1 = coords.index(elem)
        pos2 = coords.index(elem, pos1+1)
        eliminado = random.choice([pos1, pos2])
        self.aliens[eliminado].health = 0
    
    def refrescar_espacio(self):
        # se resetea la matriz, se eliminan de la lista a los aliens muertos, se detectan colisiones
        self.espacio = [[], [], [], [], []]
        self.crear_espacio()
        self.detecta_colision()
        for alien in self.aliens:
            if alien.is_alive() == False:
                self.aliens.remove(alien)
        for alien in self.aliens:
            self.espacio[alien.x][alien.y] = "*"  
              
    def crear_espacio(self):
        # se crea la matriz 5x5 con " "
        for fila in self.espacio:
            for x in range(5):
                fila.append(" ")

    def mostrar_espacio(self):
        # se muestra toda la matriz
        total = ""
        for fila in self.espacio:
            for casilla in fila:
                total += casilla
            total += "\n"
        print("_"*len(self.espacio))
        print(total)
        print("-"*len(self.espacio))
    
    def repartir_armas(self):
        # se reparten las armas en los aliens
        # si a alguien le toca Mano, sera el equivalente a no tener arma, hara daño 0
        for alien in self.aliens:
            choice = random.randint(1, 4)
            if choice == 1:
                alien.weapon = Granada(alien)
            elif choice == 2:
                alien.weapon = Canon(alien)
            elif choice == 3:
                alien.weapon = Pistola(alien)
            else:
                alien.weapon = Mano(alien)
    
    def juego(self):
        # se llaman a los metodos necesarios para iniciar el programa
        self.crear_aliens()
        self.repartir_armas()
        self.crear_espacio()
        self.refrescar_espacio()
        self.mostrar_espacio()

        
        # ciclo main del juego, hasta que haya un solo alien
        while True:
            jugador = random.choice(self.aliens)
            jugada = random.randint(1, 2)
            # el alien o dispara o se teletransporta
            if jugada == 1:
                self.espacio[jugador.x][jugador.y] = " "
                jugador.teleport()
            else:
                obj = random.choice(self.aliens)
                while jugador is obj:
                    obj = random.choice(self.aliens)
                jugador.hit(obj)
            # se detectan colisiones y se vuelve a mostrar todo
            self.refrescar_espacio()
            self.mostrar_espacio()
            # si queda 1 solo alien, se rompe el ciclo
            if len(self.aliens) == 1:
                break
        
        # se muestra la situacion final
        print("situacion final\n")
        self.refrescar_espacio()
        self.mostrar_espacio()
        

if __name__ == "__main__":
    juego = Juego()
    juego.juego()