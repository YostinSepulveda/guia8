from weapon import Weapon

class Pistola(Weapon):
    def __init__(self,duenio):
        # clase pistola hija de weapon, tiene daño
        self._damage=1
        super().__init__(duenio)

    @property
    def damage(self):
        return self._damage
        
    